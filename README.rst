===============================
Wolf Programming Language (wpl)
===============================
The Wolf Programming Language is a generic programming language with no special
features. The purpose of wolf is solely to learn how to design programming
languages, and other related projects, such as writing a compiler, virtual
machine, and IRC eval bot. Wolf is a procedural programming language, but will
also support functional programming and objected oriented programming paradigms

========
Examples
========
Examples of wolf code::

   # hello.wlf - print 'Hello, world!' to stdout and quit
   use io;

   fn main: argc, argv -> int32 {
      io.emitln(stdout, 'Hello, world!');
      ret 0;
   }

=================
Special Thanks to
=================
 * `usandfriends <https://github.com/usandfriends>`_ for markdown help (back
   when I using it)
 * `BeatrixKiddo <https://github.com/BeatrixKiddo>`_ for review
 * `TheRabbitologist <https://github.com/TheRabbitologist>`_ for testing
 * `darkf <https://github.com/darkf>`_ for the initial idea of an rpn
   calculator and general help with the project
 * `#python <http://goo.gl/J7UiPx>`_
   for general help and review
 * `#cplusplus.com <http://goo.gl/4j09II>`_ and `#osdev-offtopic
   <http://goo.gl/DTS4J8>`_ for putting up with my rants about this project
 * `Günther Starnberger <https://github.com/gstarnberger>`_ for saving my
   project
