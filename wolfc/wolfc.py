# Copyright 2015 Nicholas Chambers (nchambers) <nchambers@compilerdev.net>
#
# This file is part of wolfc.
#
# wolfc is free software: you can redistribute it and/or modify
# it under the terms of the MIT General Public License as published by
# the Massachusetts Institute of Technology.
#
# wolfc is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# MIT General Public License for more details.
#
# You should have received a copy of the MIT General Public License
# along with wolfc. If not, see <http://opensource.org/licenses/MIT>.

'''main entry point into the wolfc compiler api'''

from wolfc.lexer import lex

def comp(code):
    lexemes = lex(code)
    return lexemes
