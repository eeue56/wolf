# Copyright 2015 Nicholas Chambers (nchambers) <nchambers@compilerdev.net>
#
# This file is part of wolfc.
#
# wolfc is free software: you can redistribute it and/or modify
# it under the terms of the MIT General Public License as published by
# the Massachusetts Institute of Technology.
#
# wolfc is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# MIT General Public License for more details.
#
# You should have received a copy of the MIT General Public License
# along with wolfc. If not, see <http://opensource.org/licenses/MIT>.

'''driver for the wolfc compiler'''

import sys
from wolfc.wolfc import comp

def main(argc, argv):
    code = ''

    with open(argv[1], 'r') as handle:
        for line in handle: code += line

    print comp(code)

if __name__ == '__main__':
    main(len(sys.argv), sys.argv)
