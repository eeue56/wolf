# Copyright 2015 Nicholas Chambers (nchambers) <nchambers@compilerdev.net>
#
# This file is part of wolfc.
#
# wolfc is free software: you can redistribute it and/or modify
# it under the terms of the MIT General Public License as published by
# the Massachusetts Institute of Technology.
#
# wolfc is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# MIT General Public License for more details.
#
# You should have received a copy of the MIT General Public License
# along with wolfc. If not, see <http://opensource.org/licenses/MIT>.

'''wolfc - implementation of the wolf programming language'''

import os.path

__author__ = 'Nicholas Chambers'
__copyright__ = 'Copyright 2015 Nicholas Chambers (nchambers)'
__license__ = 'MIT'
__maintainer__ = 'Nicholas Chambers'
__email__ = 'nchambers@compilerdev.net'
__version_info__ = (2, 0, 0)
__version__ = '.'.join(map(str, __version_info__))
__description__ = 'wolfc - implementation of the wolf programming language'

basedir = os.path.dirname(os.path.realpath(__file__))
