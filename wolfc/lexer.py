# Copyright 2015 Nicholas Chambers (nchambers) <nchambers@compilerdev.net>
#
# This file is part of wolfc.
#
# wolfc is free software: you can redistribute it and/or modify
# it under the terms of the MIT General Public License as published by
# the Massachusetts Institute of Technology.
#
# wolfc is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# MIT General Public License for more details.
#
# You should have received a copy of the MIT General Public License
# along with wolfc. If not, see <http://opensource.org/licenses/MIT>.

'''Lexical Analyzer to turn wolf code into lexemes'''

_EMPTY = 0
_NUMBER = 1
_STRING = 2
_NAME = 3
_OPERATOR = 4
_COMMENT = 5

def lex(code):
    lexemes = []
    current_lexeme = ''
    state = _EMPTY

    for symbol in code:
        if symbol == '#':
            if current_lexeme:
                lexemes.append(current_lexeme)
                current_lexeme = ''

            state = _COMMENT

        elif state == _COMMENT:
            if symbol == '\n':
                state = _EMPTY

            else:
                continue

        elif symbol == '\'':
            if state == _STRING:
                current_lexeme += symbol
                lexeme.append(current_lexeme)
                current_lexeme = ''
                state = _EMPTY

            else:
                current_lexeme += symbol
                state = _STRING

        elif symbol.isspace():
            if current_lexeme:
                lexemes.append(current_lexeme)
                current_lexeme = ''
                state = _EMPTY

            continue

        elif symbol.isalpha():
            if state not in (_EMPTY, _NAME):
                lexemes.append(current_lexeme)
                current_lexeme = ''
                state = _NAME

            current_lexeme += symbol

        elif symbol.isdigit():
            if state not in (_EMPTY, _NAME, _NUMBER):
                lexemes.append(current_lexeme)
                current_lexeme = ''
                state = _NUMBER

            current_lexeme += symbol

        elif symbol in (';', ':', '=', '>', '{', '}', ',', '.', '(', ')'):
            if state not in (_EMPTY, _OPERATOR):
                lexemes.append(current_lexeme)
                current_lexeme = ''
                state = _EMPTY

            current_lexeme += symbol
