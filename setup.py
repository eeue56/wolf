#!/usr/bin/env python

from distutils.core import setup

setup(
    name='wolfc',
    version='0.0.1',
    author='Nicholas Chambers'
    author_email='nchambers@compilerdev.net',
    maintainer='Nicholas Chambers',
    maintainer_email='nchambers@compilerdev.net',
    url='',
    description='',
    long_description='',
    download_url='',
    license='MIT',
    platforms=[],
    classifiers=[
        'Environment :: Console',
        'Environment :: Web Environment',
        'Environment :: Internet Relay Chat',
        'Intended Audience :: End Users/Desktop',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT General Public License',
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: Microsoft :: Windows',
        'Operating System :: POSIX',
        'Programming Language :: Python',
    ],
    packages=['wolfc']
)
